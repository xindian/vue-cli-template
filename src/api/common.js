import http from '@/scripts/http'

/**
 * 提交反信息
 * @param {object} [query]
 * @param {string =} query.keyword
 * @param {number | string | Array<number | string> =} query.status
 * @param {number | string =} query.pageNum
 * @param {number | string =} query.pageSize
 */
export const feedBackInfo = data => {
  return http.post('/news/getList', { data })
}

/**
 * 获取新闻详情
 * @param {string} id
 */
export const getNewsDetails = id => http.get(`/news/getDetails/${id}`)
